<?php

namespace App\Helpers;

class Functions {

    public static function filterByDate($val, $end){
        $start = 0;
        switch($val) {
            case 'oneD' :
                $start = date("Y-m-d H:i:s", strtotime($end . '-1 days'));
                break;
            case 'week' :
                $start = date("Y-m-d H:i:s", strtotime($end . '-7 days'));
                break;
            case 'oneM' :
                $start = date('Y-m-d H:i:s', strtotime($end . "-1 months"));
                break;
            case 'threeM' :
                $start = date('Y-m-d H:i:s', strtotime($end . "-3 months"));
                break;
            case 'year' :
                $start = date('Y-m-d H:i:s', strtotime($end . "-1 year"));
                break;
            default :
                $start = date('Y-m-d H:i:s', strtotime($end . '-7 days'));
                break;
        }
        return $start;
    }


}