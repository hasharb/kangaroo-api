<?php

namespace App\Models;

use App\Models\Question;

class SurveyTypeResult
{

    public $type;
    public $label;
    public $result;

    private $questions = [];

    private $types = ['qcm', 'numeric', 'date'];

    function __construct($questions, $type, $label)
    {
        $this->questions = $questions;
        $this->type = $type;
        $this->label = $label;
    }

    public function aggregate()
    {
        $aggregate_function = $this->type . "_aggregate";
        $this->{$aggregate_function}();
        return $this->api_response();
    }

    private function api_response()
    {
        return [
            "type" => $this->type,
            "label" => $this->label,
            "result" => $this->result
        ];
    }

    private function qcm_aggregate()
    {
        $this->result = [];
        foreach ($this->questions as $q) {
            for ($i = 0; $i < count($q['options']); $i++) {
                if (array_key_exists($q['options'][$i], $this->result)) {
                    if ($q['answer'][$i]) $this->result[$q['options'][$i]]++;
                } else {
                    $this->result[$q['options'][$i]] = $q['answer'][$i] ? 1 : 0;
                }
            }
        }
    }

    private function numeric_aggregate()
    {
        $this->result = 0;
        $sum = array_sum(
            array_map(
                function ($item) {
                    return $item['answer'];
                },
                $this->questions
            )
        );

        $this->result = $sum / count($this->questions);
    }

    private function date_aggregate()
    {
    }
}
