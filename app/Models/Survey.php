<?php

namespace App\Models;

use App\Models\SurveyTypeResult;

class Survey
{

    public $map_data = [];
    public $result = [];
    public $code;

    function __construct($code = null)
    {
        $this->code = $code;
    }

    public function response(){
        $this->get_map_data();
        if(is_array($this->map_data) && count($this->map_data)){
            foreach ($this->map_data as $key => $r) {
                $data = [];
                $surveys = [];
                $data['code'] = $key;
                $data["name"] = $r['name'];
                foreach ($r['questions'] as $k => $vls) {
                    $survey_result = new SurveyTypeResult($vls, $k, $vls[0]['label']);
                    $survey_result = $survey_result->aggregate();
                    $surveys[] = $survey_result;
                }
                $data["survey_results"] = $surveys;
                $this->result[] = $data;
            }
        }
    }

    private function get_map_data()
    {
        for ($i = 1; $i <= 13; $i++) {
            $path = storage_path() . "/json/$i.json";
            $json = json_decode(file_get_contents($path), true);

            if ($this->code != null && $json['survey']['code'] != $this->code) continue;

            $this->map_data[$json['survey']['code']]['name'] = $json['survey']['name'];
            foreach ($json['questions'] as $q) {
                $this->map_data[$json['survey']['code']]['questions'][$q['type']][] = $q;
            }
        }
    }
}
