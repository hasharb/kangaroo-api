<?php

namespace App\Models;

class Question {
    
    public $type;
    public $label;
    public $options;
    public $answer;
}